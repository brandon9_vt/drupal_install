#!/bin/bash
# This script will install all the necessary components for a drupal 8 website 
# add php rep

rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

# install required packages
yum install -y httpd php55w php55w php55w-opcache php55w-mbstring php55w-gd php55w-xml php55-pear php55w-fpm php55w-mysql 

# start httpd service and enable service start on bootup
systemctl start httpd
systemctl enable httpd 
ln -s '/usr/lib/systemd/system/httpd.service' '/etc/systemd/system/multi-user.target.wants/httpd.service'

# modify firewall settings
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload

# install MariaDB
yum install mariadb-server mariadb

# start mysql
systemctl start mariadb
systemctl enable mariadb
ln -s '/usr/lib/systemd/system/mariadb.service' '/etc/systemd/system/multi-user.target.wants/mariadb.service'
#mysql_secure_installation
systemctl restart mariadb

curl -O https://ftp.drupal.org/files/projects/drupal-8.0.2.tar.gz
tar -zxpvf drupal-8.0.2.tar.gz
mv drupal-8.0.2 /var/www/html/drupal

cp -p /var/www/html/drupal/sites/default/default.settings.php /var/www/html/drupal/sites/default/settings.php


[ ! -d /var/www/html/drupal/sites/default/files ] && mkdir /var/www/html/drupal/sites/default/files
chmod 777 -R /var/www/html/drupal/sites/default/files

chown -R apache:apache /var/www/html/drupal
restorecon -r /var/www/html/

echo "All prerequisites installed. The drupal database and user needs to be created for mysql as well as modifying httpd.conf to allow access to the web directory."